#include <stdlib.h>
#include <assert.h>
//#include <highgui.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
//#include "opencv2/core.hpp"
#include "yuv.h"
#ifdef _WIN32
#include <direct.h>
#define GetCurrentDir _getcwd
#else  // #ifdef __unix__
#include <unistd.h>
#include <boost/progress.hpp>
#define GetCurrentDir getcwd
#endif

#define SHOW_VIDEO        0  ///> use opencv to show the video frames in the form of consecutive rgb images.
#define COUNT_FRAMES      1  ///> count total num of frames in the yuv file


int
main(int argc, char **argv) {
  std::cout << "opencv version:" << CV_VERSION << std::endl;

//#if COUNT_FRAMES
  int widthx, heightx;
  widthx = std::stoi(argv[2]);
  heightx = std::stoi(argv[3]);
  FILE *finForNumOfFrames = nullptr;
  struct YUV_Capture capForNumOfFrames{};
  enum YUV_ReturnValue retForNumOfFrames;
  finForNumOfFrames = fopen(argv[1], "rb");
  if (!finForNumOfFrames) {
    fprintf(stderr, "error: unable to open file: %s\n", argv[1]);
    return 1;
  }
  retForNumOfFrames = YUV_init(finForNumOfFrames, widthx, heightx, &capForNumOfFrames);
  if (retForNumOfFrames != YUV_OK){
    std::cout << "Value error." << std::endl;
    return EXIT_FAILURE;
  //        assert(retForNumOfFrames == YUV_OK);
  }
  // get num of frames
  int iNumOfFrames = 0;
  for (; ;iNumOfFrames++)    // pha: this is an infinite loop, will be broken by ``break;``
  {
    retForNumOfFrames = YUV_read(&capForNumOfFrames);
    if (retForNumOfFrames == YUV_EOF) {
      break;
    } else if (retForNumOfFrames == YUV_IO_ERROR) {
      fprintf(stderr, "I/O error\n");
      break;
    }
  }
  std::cout << "Total num of frames: "  << iNumOfFrames << std::endl;
//#endif

  int width, height;

  FILE *fin = nullptr;
  struct YUV_Capture cap{};
  enum YUV_ReturnValue ret;

  IplImage *bgr;

  if (argc != 4) {
    fprintf(stderr, "usage: %s file.yuv width height\n", argv[0]);
    return 1;
  }

  width = std::stoi(argv[2]);
  height = std::stoi(argv[3]);
  if (width <= 0 || height <= 0) {
    fprintf(stderr, "error: bad frame dimensions: %d x %d\n", width, height);
    return 1;
  }

  fin = fopen(argv[1], "rb");
  if (!fin) {
    fprintf(stderr, "error: unable to open file: %s\n", argv[1]);
    return 1;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  ret = YUV_init(fin, width, height, &cap);
  if (ret != YUV_OK){
    std::cout << "Value error." << std::endl;
    return EXIT_FAILURE;
  }

  bgr = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 3);
  assert(bgr);

  boost::progress_display show_progress(iNumOfFrames);

  for (int poc=0; ;poc++)    // pha: this is an infinite loop, will be broken by ``break;``
  {
    //////////////////////////////////////////////////////////////////////////////////////////////////
    ret = YUV_read(&cap);
    if (ret == YUV_EOF) {
      // ------------------------------
      // pha: ``cvWaitKey(0)`` stops your program until you press a button.
      // ------------------------------
      cvWaitKey(0);
      break;
    } else if (ret == YUV_IO_ERROR) {
      fprintf(stderr, "I/O error\n");
      break;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    cvCvtColor(cap.ycrcb, bgr, CV_YCrCb2BGR);

#if SHOW_VIDEO
    // ------------------------------
    // pha:
    // call ``cvShowImage`` in the loop thru yuv frames.
    // ------------------------------
    cvShowImage(argv[1], bgr);
    // ------------------------------
    // pha:
    // Follow up each draw with ``cvWaitKey(x)``, where x > 0.
    // This is to give time to highgui to process the draw requests from ``cvShowImage``.
    // No ``cvWaitKey(x)`` follows, nothing appears on screen.
    // It also means after 35 milliseconds, a new frame will be shown.
    // Just a type of delay.
    cvWaitKey(35);
#endif
    // ---------------------------------
    // pha: use this to save IplImage* to png file!
    // ---------------------------------
    //////////////////////////////////////////////////////////////////////////////////////////////////
    int iPoc = poc;
    char szFileName[FILENAME_MAX];
    // get current working directory
    char* predNextFramePath = GetCurrentDir(szFileName, sizeof(szFileName));
    char partOfName[FILENAME_MAX];
    sprintf(partOfName, "/images/img_%d.jpg", iPoc);
    strcat(predNextFramePath, partOfName);
    cvSaveImage(predNextFramePath, bgr);

    ++show_progress;
  }
#if SHOW_VIDEO
  cvWaitKey(0);
#endif
  std::cout << "Success." << std::endl;
  return 0;
}