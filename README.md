opencv-yuv
==========

Loading YUV videos with OpenCV.

Usage
-----
``opencv-yuv`` is the executable binary after compilation. 
You need to specify yuv path and yuv width height.

    opencv-yuv sample/foreman-short-cif.yuv 352 288
    opencv-yuv sample/rec.yuv 416 240


    
    
    
    
```shell
/media/pharrell/seagate2t/archive/sequences/Traffic.yuv 2560 1600


```